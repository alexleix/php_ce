<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>用户中心</title>
</head>
<body>
<h1>用户中心</h1>

当前登陆用户id: {{ user_id }}
<a href="{{ url('user/exitlogin') }}">退出登陆</a>
</body>
</html>


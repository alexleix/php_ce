<?php

namespace user\model;

/**
 * 模型基类
 * Class Model
 * @package user\model
 */
class Model extends \Phalcon\Mvc\Model
{

    public $_prefix = 'user_';

    public function initialize()
    {
        $this->setSource($this->_prefix . \get_class_name(get_class($this)));
    }
}
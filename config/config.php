<?php

return [
    'view' => [
        'viewsdir' => APP_DIR . '/view/'
    ],
    'cache' => [
        'lifetime' => 3600,
        'backend' => 'File',
        'options' => [
            'cacheDir' => RUNTIME_DIR . '/cache/',
        ]
    ],
    'baseurl' => '/',
    'db' => [
        'host' => '172.16.3.137',
        'username' => 'root',
        'password' => '123.369',
        'dbname' => 'xssg',
        'port' => 33306,
        'options' => [
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'UTF8'",
            PDO::ATTR_CASE => PDO::CASE_LOWER
        ]
    ]
];
<?php

namespace xssg\sys\middleware;


use xssg\tool\Trace;

class Volt
{
    public function compileStatement($statement)
    {
        Trace::record('volt', __FUNCTION__);
        if ($statement['type'] === 357) {

            if ($statement['value'] ? strpos($statement['value'], '<?') : false) {
                pr($statement);
                throw new \Phalcon\Mvc\View\Exception("不允许出现PHP语法!在 " . $statement["file"] . " on line " . $statement["line"]);

            }
        }
    }

    public function compileFunction()
    {
        Trace::record('volt', __FUNCTION__);
        //dump(func_get_args());
    }

    public function compileFilter()
    {
        Trace::record('volt', __FUNCTION__);
        //dump(func_get_args());
    }

    public function resolveExpression()
    {
        Trace::record('volt', __FUNCTION__);
        //dump(func_get_args());
    }
}
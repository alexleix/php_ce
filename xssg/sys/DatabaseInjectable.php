<?php

namespace Xssg\Sys;

interface DatabaseInjectable
{

    abstract public static function getData();
}
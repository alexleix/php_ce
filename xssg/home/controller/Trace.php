<?php

namespace Xssg\Home\Controller;

class Trace extends \Xssg\Sys\Controller
{

    public function index()
    {
        $id = $this->request->getQuery('id');
        $data = $this->cache->get($id);
        $this->view->setVar("data", $data);
    }
}
<?php

namespace Xssg\user\controller;

use xssg\sys\Controller;
use xssg\tool\Alert;

/**
 * 用户控制器
 * Class user
 * @package Xssg\home\controller
 */
class User extends Controller
{
    /**
     * 注册
     * @return mixed
     */
    public function reg()
    {
        $username = $this->request->getPost('username', 'string');
        $password = $this->request->getPost('password', 'string');
        $password2 = $this->request->getPost('password2', 'string');
        $User = new \user\User();
        $re = $User->reg($username, $password, $password2);
        if (is_int($re)) {
            $re2 = Alert::success('注册成功!', '成功注册用户!');
        } else {
            $re2 = Alert::error('注册失败!!', '注册失败!!' . $re);
        }
        return $re2;
    }

    /**
     * 登陆
     */
    public function login()
    {
        $username = $this->request->getPost('username', 'string');
        $password = $this->request->getPost('password', 'string');
        $User = new \user\User();
        $re = $User->login($username, $password);
        if (is_int($re)) {
            $this->session->destroy();
            $this->session->start();
            $this->session->set('user_id', $re);
            $re2 = Alert::success('登陆成功!', '成功注册用户!');
        } else {
            $re2 = Alert::error('登陆失败!!', '注册失败!!' . $re);
        }
        return $re2;
    }

}